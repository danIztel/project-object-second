/*
    TASK 1

    Напишите функцию конструктор, которая создает объект 'comment' со свойствами :
    - id
    - author
    - text

    Создайте с помощью этого конструктора минимум 3 объекта
*/
function Comment(id, author, text) {
    this.id = id;
    this.author = author;
    this.text = text;
}

const comment1 = new Comment(100, "Dan Brown", "Da Vinci Code");
const comment2 = new Comment(200, "Dan Black", "Da Vincili Code");
const comment3 = new Comment(300, "Dan Red", "Da Vincivi Code");

console.log(comment1);
console.log(comment2);
console.log(comment3);


/*
    TASK 2

    var rex = {
        animal: 'dog',
        makeSound: function(){
            console.log('Гав Гав');
        }
    };

    var charlie = {
        animal: 'dog',
        runFast : function() {
            console.log('I can run very fast');
        }
    }

    var martin = {
        animal: 'dog'
    }

    У нас есть три собаки. Собака rex умеет лаять, собака charlie умеет быстро бегать, а вот собака martin к сожалению ничего из этого не может.
    Научите собаку charlie лаять, а собаку martin и лаять и быстро бегать.

*/
const rex = {
    animal: 'dog',
    makeSound: function(){
        console.log('Гав Гав');
    }
};
const charlie = {
    animal: 'dog',
    runFast : function() {
        console.log('I can run very fast');
    }
}
const martin = {
    animal: 'dog'
}

rex.__proto__ = charlie;
charlie.__proto__ = martin;
console.log(martin);
console.log(charlie);

/*
    TASK 3

    Создайте объект работника с именем Евгений, которому 33 года, который работает дизайнером, и у которого зарплата 2500$ в месяц.
    Сделайте так, чтобы при проверке этого задания я не смог с помощью перебора свойств этого объекта узнать какая зарплата у этого работника.
    Само свойство зарпалат, и его значение должно присутствовать в объекте.

*/
const evgeniy = Object.create({},{
    name: {
        value:"Evgeniy Brown",
        enumerable: true
    },
    age: {
        value: 33,
        enumerable: true
    },
    salary: {
        value: "$2500",
        enumerable: true
    },
    job: {
        value: "designer",
        enumerable: true
    }
});

for(let key in evgeniy){
    console.log(key);
}



/*

    TASK 4

    Выведите на экран текущую дату-время в формате '00:00:00 31.12.2010'.
    Также напишите функцию, которая будет добавлять 0 перед днями и месяцами, которые состоят из одной цифры (из 1.9.2014 сделает 01.09.2014)

*/

const date = new Date();
function createNull(num){
    if (num < 10) {
        return '0' + num;
    }
   return num;

};
console.log(`${date.getHours()}:${date.getMinutes()}:${date.getSeconds()} ${createNull(date.getDate())}:${createNull(date.getMonth() + 1)}:${date.getFullYear()}`);


